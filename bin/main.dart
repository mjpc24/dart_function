num add(num x, num y) {
    return x + y;
}

Map<String, num> switchNumber(num num1, num2) {
    
    return {
        'num1': num2 , 'num2': num1
    };

}

int? countLetter(String letter, String sentence) {
    int output = 0;
    if (letter.length > 1){
        return null;
    } else {
        for (var i = 0;i< sentence.length;i++){
            if(sentence[i] == letter){
                output++;
            }
        }
    }
    return output;
}

bool isPalindrome(String text) {
    text = text.split(' ').join('').toLowerCase();

        for (var i = 0; i < text.length/2; i++) {
            if (text[i] != text[text.length - 1 - i]) {
                print(text[i]);
                print(text[text.length -1 - i ]);
                return false;
            }
        }
        return true;

}

bool isIsogram(String text) {
    var char = [];
        for(var i = 0; i < text.length; i++){
            if(char.indexOf(text[i]) != -1){
                return false;
            } else {
                char.add(text[i]);
            }
        }
        return true;
}

num? purchase(num age, num price) {

    var disc= price * 0.8;
    if(age < 13){
        return null;
    } else if((age >= 13 && age <= 21) || age >= 65){
        return disc;
    } else {
        return num.parse(price.toStringAsFixed(2));
    }

}

List<String> findHotCategories(List items) {

    List<String> hotCategories = [];
        items.forEach((item) {
            if(item['stocks'] == 0){
                if(hotCategories.indexOf(item['category']) == -1){
                    hotCategories.add(item['category']);
                }
            }
        });
        return hotCategories ;
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].

}

List<String> findFlyingVoters(List candidateA, List candidateB) {
    List<String> flyingVoters = [];

        candidateA.forEach((voteA) {
            if(candidateB.indexOf(voteA) != -1){
                flyingVoters.add(voteA);
            }
        });

        return flyingVoters;
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].

    
}

List<Map<String, dynamic>> calculateAdEfficiency(List items) {
        
        Iterable<Map<String, dynamic>> adEfficiencies = items.map((campaign) {
            return {
                'brand': campaign['brand'],
                'adEfficiency': (campaign['customersGained']/campaign['expenditure']) * 100
            };
        }); 
        List<Map<String, dynamic>> test = adEfficiencies.toList();
        test.sort((a,b) => (b['adEfficiency']).compareTo(a['adEfficiency']));
        
        return test;
    // ((campaignA, campaignB) {(campaignB.adEfficiency - campaignA.adEfficiency);} )
    // Sort the ad efficiency according to the most to least efficient.
    // The passed campaigns array from the test are the following:
    // { brand: 'Brand X', expenditure: 12345.89, customersGained: 4879 }
    // { brand: 'Brand Y', expenditure: 22456.17, customersGained: 6752 }
    // { brand: 'Brand Z', expenditure: 18745.36, customersGained: 5823 }
    // The efficiency is computed as (customersGained / expenditure) x 100.
    // The expected output after processing the campaigns array are:
    // { brand: 'Brand X', adEfficiency: 39.51922461645131 }
    // { brand: 'Brand Z', adEfficiency: 31.063687227132476 }
    // { brand: 'Brand Y', adEfficiency: 30.06746030155632 }

}
void item(element) {
}
